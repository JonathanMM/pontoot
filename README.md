PonTOOT is a nodejs application to copy tweets into Mastodon

Install
=======

Note : Need node.js

On debian/ubuntu
```bash
sudo apt install nodejs
npm install
```

Run
===

```bash
npm start
```
On the first call, the script ask required informations to works. Then, for each
call, the script see there are new tweets (except responses and RT) and publish
one toot on Mastodon. I recommand to call it on cron task.

If you want to synchronise many users, you can run :
```bash
node web
```
then, go to http://localhost:8080/ and follow steps.