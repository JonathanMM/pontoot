const fs = require('fs');
const prompt = require('prompt');
const http = require('http');
const htmlDecode = require('js-htmlencode').htmlDecode;

fs.readFile('config', (err, data) => {
    if(err || data.length == 0)
        demandeServer();
    else
    {
        var infos = JSON.parse(data);
        if(infos.users == undefined || infos.users[0].client_id == undefined)
            demandeCreds(infos);
        else if(infos.users[0].token == undefined)
            getOauthToken(infos);
        else if(infos.users[0].accessToken == undefined)
            getTokenUser(infos);
        else if(infos.twitterConsumerkey == undefined)
            demanderTwitter(infos);
        else
            initProcess(infos);
    }
});

//Ask server address
function demandeServer()
{
    var schema = {
        properties: {
            server: {
                type: 'string',
                required: true,
                description: 'Server (with http(s):// at the begin)'
            }

        }
    };
    prompt.start();
    prompt.get(schema, function (err, result) {
        if (err) { return onErr(err); }
        var infos = {users: [{server: result.server}]};
        fs.writeFile('config', JSON.stringify(infos));
        demandeCreds(infos);
    });
}

//Ask creditentials for the app
function demandeCreds(infos)
{
    //Analyze adress
    var adress = infos.users[0].server;
    var protocolSplit = adress.split(':');

    var https;
    var port;
    if(protocolSplit[0] == 'http')
    {
        https = require('http');
        port = 80;
    }
    else
    {
        https = require('https');
        port = 443;
    }

    var hostSplit = protocolSplit[1].split('/'); // format of adress : //host/
    var host = hostSplit[2];

    var options = {
        host: host,
        path: '/api/v1/apps',
        port: port,
        method: 'POST'
    };

    callback = function(response) {
        var str = ''
        response.on('data', function (chunk) {
            str += chunk;
        });

        response.on('end', function () {
            var infosRequest = JSON.parse(str);
            infos.users[0].client_id = infosRequest.client_id;
            infos.users[0].client_secret = infosRequest.client_secret;
            fs.writeFile('config', JSON.stringify(infos));
            getOauthToken(infos);
        });
    }

    var post = {
        client_name: 'PonTOOT',
        redirect_uris: 'urn:ietf:wg:oauth:2.0:oob',
        scopes: 'read write'
    };

    var postArray = [];
    for(var key in post)
    {
        postArray.push(key + '=' + post[key]);
    }
    var postString = postArray.join('&');

    var req = https.request(options, callback);
    req.write(postString);
    req.end();
}

//Ask Oauth token
function getOauthToken(infos)
{
    console.log("Go to : "+infos.users[0].server+"/oauth/authorize?client_id="+infos.users[0].client_id+"&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code&scope=read+write");
    console.log("Allow the application and copy-paste the token");
    var schema = {
        properties: {
            userToken: {
                type: 'string',
                required: true,
                description: 'Token'
            }

        }
    };
    prompt.start();
    prompt.get(schema, function (err, result) {
        if (err) { return onErr(err); }
        infos.users[0].token = result.userToken;
        fs.writeFile('config', JSON.stringify(infos));
        getTokenUser(infos);
    });
}

//Ask token of user on Oauth
function getTokenUser(infos)
{
    var OAuth2 = require('oauth').OAuth2;

    var oauth = new OAuth2(infos.users[0].client_id, infos.users[0].client_secret, infos.users[0].server, null, '/oauth/token');
    var url = oauth.getAuthorizeUrl({ redirect_uri: 'urn:ietf:wg:oauth:2.0:oob', response_type: 'code', scope: 'read write' });

    oauth.getOAuthAccessToken(infos.users[0].token, { grant_type: 'authorization_code', redirect_uri: 'urn:ietf:wg:oauth:2.0:oob' }, function(err, accessToken, refreshToken, res) {
        infos.users[0].accessToken = accessToken;
        fs.writeFile('config', JSON.stringify(infos));
        demanderTwitter(infos);
    })
}

//Ask Twitter Creds
function demanderTwitter(infos)
{
    console.log("Go to : https://apps.twitter.com");
    console.log("Create a new application, then, go to Keys and access token tab");
    console.log("Click on Create an access token, and copy-paste values");
    var schema = {
        properties: {
            consumerKey : {
                type: 'string',
                required: true,
                description: 'Consumer Key (API Key)'
            },
            consumerSecret : {
                type: 'string',
                required: true,
                description: 'Consumer Secret (API Secret)'
            },
            accessToken : {
                type: 'string',
                required: true,
                description: 'Access Token'
            },
            accessTokenSecret : {
                type: 'string',
                required: true,
                description: 'Access Token Secret'
            }
        }
    };
    prompt.start();
    prompt.get(schema, function (err, result) {
        if (err) { return onErr(err); }
        infos.twitterConsumerkey = result.consumerKey;
        infos.twitterConsumersecret = result.consumerSecret;
        infos.users[0].twitterToken = result.accessToken;
        infos.users[0].twitterTokenSecret = result.accessTokenSecret;
        fs.writeFile('config', JSON.stringify(infos));
        initProcess(infos);
    });
}

//Start process
let cacheToots;
function initProcess(infos)
{
    cacheToots = {};
    for(var i in infos.users)
        goToTwitter(infos, i);
}

function goToTwitter(infos, indexUser)
{
    var Twitter = require('twitter');

    var client = new Twitter({
        consumer_key: infos.twitterConsumerkey,
        consumer_secret: infos.twitterConsumersecret,
        access_token_key: infos.users[indexUser].twitterToken,
        access_token_secret: infos.users[indexUser].twitterTokenSecret
    });

    cacheToots[indexUser] = [];
    client.get("statuses/user_timeline", {count: 5, tweet_mode: 'extended'})
        .then(tweets => {
        var n = tweets.length;
        var i = 0;
        var tweetPosted = null;
        while(i < n)
        {
            var tweet = tweets[i];
            //console.log(tweet);
            if(tweet.retweeted_status == undefined && (tweet.entities.user_mentions == undefined || tweet.entities.user_mentions.length == 0 || tweet.in_reply_to_status_id_str == null)) //Remove RT and reply
            {
                if(infos.users[indexUser].idLastTweet == tweet.id_str)
                    i = n;
                else
                {
                    if(tweetPosted == null)
                        tweetPosted = tweet.id_str;
                    prepareTootForMastodon(getMediaFromTweet(tweet), indexUser);
                }
            }
            i++;
        }
        
        if(tweetPosted != null)
        {
            infos.users[indexUser].idLastTweet = tweetPosted;
            fs.writeFile('config', JSON.stringify(infos));
        } else //No found
            console.log("No new tweet");

        publishAllOnMastodon(infos, indexUser);
    })
        .catch(error => {
        console.log(error);
    });
}

function prepareTootForMastodon(toot, indexUser)
{
    if(toot.url == undefined || toot.url.length == 0)
    {
        let childTootIndex = cacheToots[indexUser].findIndex(t => (toot.id == t.reply && (t.url == undefined || t.url.length == 0)));
        if(childTootIndex != -1)
        {
            let childToot = cacheToots[indexUser][childTootIndex];
            let fullToot = toot.text + ' ' + childToot.text;
            if(fullToot.length <= 500)
            {
                toot.text = fullToot;
                cacheToots[indexUser][childTootIndex] = toot;
            } else
                cacheToots[indexUser].push(toot);
        } else
            cacheToots[indexUser].push(toot);
    } else
        cacheToots[indexUser].push(toot);
}

function publishAllOnMastodon(infos, indexUser)
{
    var Masto = require('mastodon');

    var M = new Masto({
        access_token: infos.users[indexUser].accessToken,
        timeout_ms: 60*1000,
        api_url: infos.users[indexUser].server+'/api/v1/',
    });

    for(let toot of cacheToots[indexUser])
        postOnMastodon(M, toot);
}

function getMediaFromTweet(tweet)
{
    let objReturn = {
        id: tweet.id_str,
        reply: tweet.in_reply_to_status_id_str
    };
    if(tweet.entities == undefined)
    {
        objReturn.url = [];
        objReturn.text = htmlDecode(tweet.full_text);
        return objReturn;
    }

    let photos = [];
    let text = tweet.full_text;
    if(tweet.extended_entities != undefined && tweet.extended_entities.media != undefined)
    {
        for(var i in tweet.extended_entities.media)
        {
            let media = tweet.extended_entities.media[i];
            if(media.type == "photo")
            {
                let urlPhoto = media.media_url;
                photos.push(urlPhoto);
                let urlDisplay = media.url;
                text = text.replace(urlDisplay, '');
            }
        }
    }

    if(tweet.entities.urls != undefined)
    {
        for(var i in tweet.entities.urls)
        {
            let url = tweet.entities.urls[i];
            let shortUrl = url.url;
            let fullUrl = url.expanded_url;
            text = text.replace(shortUrl, fullUrl);
        }
    }
    objReturn.url = photos;
    objReturn.text = htmlDecode(text.trim());
    return objReturn;
}

function postOnMastodon(M, content)
{
    var photos = content.url;
    var text = content.text;

    if(photos != undefined && photos.length > 0)
    {
        let photosPromise = photos.map((url, indexFile) => {
            var file = fs.createWriteStream("tmp"+indexFile+".jpg");
            return new Promise((resolve, reject) => {
                http.get(url, res => {
                    file.on('close', () => {
                        resolve(indexFile);
                    });
                    res.pipe(file);
                });
            }).then(indexFile => new Promise((resolve, reject) => {
                return M.post('media', { file: fs.createReadStream("tmp"+indexFile+".jpg") }).then(resp => {
                    fs.unlink("tmp"+indexFile+".jpg");
                    return resolve(resp.data.id);
                }).catch(err => {return reject(err)});
            }))
        });
        Promise.all(photosPromise).then(photosId => {
            return M.post('statuses', { status: text, media_ids: photosId })
        }).catch(err => {console.log(JSON.stringify(err))});
    } else
        M.post('statuses', {status: text}).then(resp => console.log("Posted on Mastodon !")).catch(error => console.log(error));
}
