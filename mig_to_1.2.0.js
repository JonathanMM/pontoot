const fs = require('fs');

fs.readFile('config', (err, data) => {
    if(!err && data.length != 0)
    {
        var infos = JSON.parse(data);
        if(infos.users == undefined)
        {
            let newInfo = {};
            newInfo.twitterConsumerkey = infos.twitterConsumerkey;
            newInfo.twitterConsumersecret = infos.twitterConsumersecret;
            newInfo.users = [
                {
                    server: infos.server,
                    client_id: infos.client_id,
                    client_secret: infos.client_secret,
                    token: infos.token,
                    accessToken: infos.accessToken,
                    twitterToken: infos.twitterToken,
                    twitterTokenSecret: infos.twitterTokenSecret,
                    idLastTweet: infos.idLastTweet
                }
            ];
            fs.writeFile('config', JSON.stringify(newInfo));
        }
        console.log('converted');
    }
});
