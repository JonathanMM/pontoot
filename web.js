const fs = require('fs');
const prompt = require('prompt');
const express = require('express');
const OAuth = require("oauth");

var indexUser = -1;

const serverInstance = 'http://gsv95.nocle.fr:8080'; //Change it to the URL, root of callback for authentification function

function demandeCreds(infos)
{
	//Analyze adress
	var adress = infos.server;
	var protocolSplit = adress.split(':');

	var https;
	var port;
	if(protocolSplit[0] == 'http')
	{
		https = require('http');
		port = 80;
	}
	else
	{
		https = require('https');
		port = 443;
	}

	var hostSplit = protocolSplit[1].split('/'); // format of adress : //host/
	var host = hostSplit[2];

	var options = {
		host: host,
		path: '/api/v1/apps',
		port: port,
		method: 'POST'
	};

	callback = function(response) {
		var str = ''
		response.on('data', function (chunk) {
			str += chunk;
		});

		response.on('end', function () {
			var infosRequest = JSON.parse(str);

			fs.readFile('config', (err, data) => {
				let infosStock = JSON.parse(data);
				infosStock.users[indexUser].client_id = infosRequest.client_id;
				infosStock.users[indexUser].client_secret = infosRequest.client_secret;
				infosStock.users[indexUser].server = infos.server;
				fs.writeFile('config', JSON.stringify(infosStock));

				infos.res.redirect(infos.server+"/oauth/authorize?client_id="+infosRequest.client_id+"&redirect_uri="+serverInstance + "/mastodon/callback&response_type=code&scope=read+write")
			})
		});
	}

	var post = {
		client_name: 'PonTOOT',
		redirect_uris: serverInstance + '/mastodon/callback',
		scopes: 'read write',
		website: 'https://framagit.org/JonathanMM/pontoot'
	};

	var postArray = [];
	for(var key in post)
	{
		postArray.push(key + '=' + post[key]);
	}
	var postString = postArray.join('&');

	var req = https.request(options, callback);
	req.write(postString);
	req.end();
}

//Ask token of user on Oauth
function getTokenUser(infos, res)
{
	var OAuth2 = require('oauth').OAuth2;

	var oauth = new OAuth2(infos.users[indexUser].client_id, infos.users[indexUser].client_secret, infos.users[indexUser].server, null, '/oauth/token');
	var url = oauth.getAuthorizeUrl({ redirect_uri: serverInstance + '/mastodon/callback', response_type: 'code', scope: 'read write' });

	oauth.getOAuthAccessToken(infos.users[indexUser].token, { grant_type: 'authorization_code', redirect_uri: serverInstance + '/mastodon/callback' }, function(err, accessToken, refreshToken, results) {
		infos.users[indexUser].accessToken = accessToken;
		fs.writeFile('config', JSON.stringify(infos));
		res.send("Config OK !");
	})
}

fs.readFile('config', (err, data) => {
	var infos = {};
	var oauth;
	if(err || data.length == 0)
		console.log('Configuration not found.');
	else {
		infos = JSON.parse(data);
		if(infos.twitterConsumerkey == undefined)
			console.log("Need twitter creditentials");
		else {
			oauth = new OAuth.OAuth(
				'https://api.twitter.com/oauth/request_token',
				'https://api.twitter.com/oauth/access_token',
				infos.twitterConsumerkey,
				infos.twitterConsumersecret,
				'1.0A',
				null,
				'HMAC-SHA1'
			);
		}
	}

	let app = express();
	let infosOauth = {};

	var bodyParser = require('body-parser');
	app.use(bodyParser.urlencoded({extended: true}));

	app.get('/', (req, res) => {
		res.send('<a href="/twitter/connect">Connect on twitter</a>');
	})

	app.get('/conf', (req, res) => {
		if(err || data.length == 0 || infos.twitterConsumerkey == undefined)
			res.send('<form method="post" action="/conf">Consumer key : <input name="consumerKey" /><br />Consumer secret : <input name="consumerSecret" /><br /><input type="submit" value="Send" /></form>');
		else
			res.redirect('/');
	});

	app.post('/conf', (req, res) => {
		var infos = {};
		infos.twitterConsumerkey = req.body.consumerKey;
		infos.twitterConsumersecret = req.body.consumerSecret;
		fs.writeFile('config', JSON.stringify(infos));
		oauth = new OAuth.OAuth(
			'https://api.twitter.com/oauth/request_token',
			'https://api.twitter.com/oauth/access_token',
			infos.twitterConsumerkey,
			infos.twitterConsumersecret,
			'1.0A',
			null,
			'HMAC-SHA1'
		);
		res.redirect('/');
	});

	app.get('/twitter/connect', (req, res) => {
		oauth.getOAuthRequestToken((error, oauth_token, oauth_token_secret, results) => {
			if (error)
				res.send("Error during asking token to twitter : " + JSON.stringify(error));
			else {
				infosOauth.oauthRequestToken = oauth_token;
				infosOauth.oauthRequestSecret = oauth_token_secret;

				res.redirect('https://twitter.com/oauth/authenticate?oauth_token='+oauth_token)
			}
		});
	});

	app.get('/twitter/callback', (req, res) => {
		oauth.getOAuthAccessToken(
			infosOauth.oauthRequestToken,
			infosOauth.oauthRequestSecret,
			req.query.oauth_verifier,
			(error, oauthAccessToken, oauthAccessTokenSecret, results) =>
			{
				if(error)
					res.send("Error on callback : " + error);
				else
				{
					fs.readFile('config', (err, data) => {
						let infosUser = JSON.parse(data);
						//Create a new user
						indexUser = infosUser.users.length;
						infosUser.users[indexUser] = {};
						infosUser.users[indexUser].twitterToken = oauthAccessToken;
						infosUser.users[indexUser].twitterTokenSecret = oauthAccessTokenSecret;
						fs.writeFile('config', JSON.stringify(infosUser));
						res.redirect('/mastodon');
					})
				}
			})
	});

	app.get('/mastodon', (req, res) => {
		res.send('<form method="post" action="/mastodon/connect">Server (with http(s):// at the begin) : <input name="server" /><input type="submit" value="Send" /></form>');
	});

	app.post('/mastodon/connect', (req, res) => {
		demandeCreds({res: res, server: req.body.server});
	});

	app.get('/mastodon/callback', (req, res) => {
		fs.readFile('config', (err, data) => {
			infos = JSON.parse(data);
			infos.users[indexUser].token = req.query.code;
			fs.writeFile('config', JSON.stringify(infos));
			getTokenUser(infos, res);
		})
	});

	app.listen(process.env.PORT || 8080);
})
